// space

/**
 * Pipedrive communicator
 *
 * The purpose of this class is to organise communication with pipedrive api
 */

const API_TOKEN = "972b32e979546d7066167e5bfbc0845bd7beaffd";

export default new class PdCommunicator {
  /**
   * Init the router
   *
   * use the scope param to bind $hppt, to make http calls with vue-resourse
   * @param scope
   */
  init(scope) {
    this.$http = scope.$http;
  }

  getUsers() {
    return this.$http
      .get(
        "https://api.pipedrive.com/v1/persons?start=0&api_token=" + API_TOKEN
      )

      .then(async res => {
        let usersData = res.data.data;

        //add random profile image src
        await this.applyProfileImage(usersData);

        return usersData;
      })

      .catch(err => {
        console.err("Error fetching users");
      });
  }

  applyProfileImage = async users => {
    const pics = (await this.getUserPics(users.length)) || [];

    users.forEach(
      (user, index) => (user.pictureSrc = pics[index].picture.large)
    );
  };

  getUserPics = async picCount => {
    const data = await this.$http.get(
      "https://randomuser.me/api/?results=" + picCount.toString()
    );
    return data.data.results;
  };
}();
